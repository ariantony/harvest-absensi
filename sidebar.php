<ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo url('index.php')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item dropdown" id="fingerprint_">
        <a class="nav-link dropdown-toggle" id="fingerprint" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>FingerPrint</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" id="fingerprint_add" href="<?php echo url('master/fingerprint/add.php'); ?>">Tambah</a>
          <a class="dropdown-item" id="fingerprint_list" href="<?php echo url('master/fingerprint/list.php'); ?>">List</a>
        </div>
      </li>
      <li class="nav-item" id="logdata">
        <a class="nav-link" href="<?php echo url('logdata/list.php'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Log Data</span></a>
      </li>
      <li class="nav-item" id="logalldata">
        <a class="nav-link" href="<?php echo url('logdata/log_all.php'); ?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Log Data ALL</span></a>
      </li>
    </ul>
