<?php 
    session_start();
    if (!$_SESSION['login']) {
        header('location:'.url('login/login.php'));
    }

	function url($url)
	{
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/absen/basima/';
		return $host.$url ;
	}

	function Parse_Data($data,$p1,$p2){
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;	
	}

	date_default_timezone_set("Asia/Jakarta");
	
	function convert($size)
	{
	    $unit=array('b','kb','mb','gb','tb','pb');
	    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}
	include 'connect.php';
	
 ?>