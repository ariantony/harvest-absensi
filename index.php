<!DOCTYPE html>
<html lang="en">
  <?php include 'config.php'; ?>
  <?php include 'header.php'; ?>
<body id="page-top">
  <?php include 'navbar.php'; ?>
  <?php 
        $sql1 = "SELECT COUNT(*) AS device  FROM tbl_hardware";
        $result1 = mysqli_query($conn,$sql1);
	 foreach ($result1 as $key) {
        	$tbl_hardware = $key['device'];
	} 

        $sql2 = "SELECT COUNT(id_log) AS log FROM tbl_log";
        $result2 = mysqli_query($conn,$sql2);
        foreach ($result2 as $key) {
         $tbl_log = $key['log'];
        }

        $sql3 = "SELECT * FROM tbl_user";
        $result3 = mysqli_query($conn,$sql3);
        $tbl_user = mysqli_num_rows($result3);

        $sql2 = "SELECT COUNT(DISTINCT pin) AS pin FROM tbl_log";
        $result2 = mysqli_query($conn,$sql2);
        foreach ($result2 as $key) {
         $pin = $key['pin'];
        }

    ?>
  <div id="wrapper">

    <!-- Sidebar -->
  <?php include 'sidebar.php'; ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">index.php</li>
        </ol>

        <!-- Page Content -->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"><?php echo $tbl_hardware; ?> Device!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo url('master/fingerprint/list.php'); ?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"><?php echo $tbl_log; ?> Total Log!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?php echo url('logdata/log_all.php'); ?>">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5"><?php echo $tbl_user; ?> User!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5"><?php echo $pin; ?> Personil!</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include 'footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
     <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>
</body>

</html>
<?php //echo "Memory Usage Index: ".convert(memory_get_usage(true)); ?>
