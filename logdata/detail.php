<!DOCTYPE html>
<html lang="en">
  <?php include '../config.php'; ?>
  <?php include '../header.php'; ?>
<body id="page-top">
  <?php include '../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../sidebar.php'; ?>
   	<?php if (isset($_GET['id_fp'])) {
          $id_fp = $_GET['id_fp'];
          $sql = "SELECT h.*,s.last_update,s.status FROM tbl_hardware h LEFT JOIN tbl_status s ON h.id_fp=s.id_fp WHERE h.id_fp = '$id_fp' ORDER BY h.ip ";
          $query = mysqli_query($conn,$sql);
          foreach ($query as $key) {
            $ip = $key['ip'];
            $c_key = $key['c_key'];
            $number_fp = $key['number_fp'];
            $desc = $key['desc'];
            $outlet = $key['outlet'];
            $type = $key['type'];
            $machine = $key['machine'];
            $last_update = $key['last_update'];
            $status = $key['status'];
            $run_interval = $key['run_interval'];
          }
    	}
    ?>
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="log.php">Log FingerPrint</a>
          </li>
          <li class="breadcrumb-item active">Log</li>
        </ol>
		<div class="card mb-3">
       
            <div class="card-header">
              FingerPrint              
            </div>
          <div class="card-body">
            <?php if (isset($_GET['status'])): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Perhatian! : </strong> Data Berhasil Ditambahkan
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
          <?php endif ?>
          <?php if (isset($_GET['data'])): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong> Tidak ada data baru di server.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            <?php endif ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID Device</th>
                  <th><?php echo $id_fp; ?></th>
                </tr>
                <tr>
                  <th>Outlet</th>
                  <th><?php echo $outlet; ?></th>
                </tr>
                <tr>
                  <th>Device</th>
                  <th><?php echo $machine; ?></th>
                </tr>
                <tr>
                  <th>Type</th>
                  <th><?php echo $type; ?></th>
                </tr>
                <tr>
                  <th>Alamat IP</th>
                  <th><?php echo $ip; ?></th>
                </tr>
                <tr>
                  <th>Common Key</th>
                  <th><?php echo $c_key; ?></th>
                </tr>
                <tr>
                  <th>Last Update</th>
                  <th><?php echo $last_update; ?></th>
                </tr>
                <tr>
                  <th>Status</th>
                  <th><?php echo $status; ?></th>
                </tr>
                <tr>
                  <th>Nomor Seri</th>
                  <th><?php echo $number_fp; ?></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <!-- Page Content -->
        <div class="card mb-3">
           <div class="card-header">
            <i class="fas fa-table"></i>
            Log Data FingerPrint 
	            <div style="float: right;">
	            	<a class="btn btn-success btn-sm" href="get_log.php?id_fp=<?php if (isset($_GET['id_fp'])) { echo $id_fp; }?>"> <i class="fas fa-download"></i> Get Log</a>
	            	<a class="btn btn-success btn-sm" href="list.php"> <i class="fas fa-list"></i> List</a>
                <a class="btn btn-success btn-sm" href=""> <i class="fas fa-recycle"></i> Refresh</a>
	            </div>
        	</div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>PIN</th>
                    <th>DateTime</th>
                    <th>Verified</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
               	<?php 
                    $sql = "SELECT * FROM tbl_log WHERE id_fp = '$id_fp'";
                    $result = mysqli_query($conn,$sql);
                    $row = mysqli_num_rows($result);
                    if ($row > 0 ) {
                        while ($k = mysqli_fetch_array($result)) {
                        echo "<tr>
                                <td class='hidden-phone'>".$k['id_log']."</td>
                                <td class='hidden-phone'>".$k['pin']."</td>
                                <td class='hidden-phone'>".$k['datetime']."</td>
                                <td class='hidden-phone'>".$k['verified']."</td>
                                <td class='hidden-phone'>".$k['status']."</td>
                              </tr>";
                        }
                    }

             	?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../asset/vendor/datatables/jquery.dataTables.js"></script>
  <script src="../asset/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../asset/js/demo/datatables-demo.js"></script>
	<script type="text/javascript">
     	$(document).ready(function() {
	      $('#logdata').addClass('active');

	    });
        function deleteModal(id_fp) {
  
         $('#deleteModal').modal('show');
         $("#delete_").attr("href", 'delete.php?id_fp='+id_fp);
        }
        
 	</script>
</body>

</html>
