<!DOCTYPE html>
<html lang="en">
  <?php include '../config.php'; ?>
  <?php include '../header.php'; ?>
<body id="page-top">
  <?php include '../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../sidebar.php'; ?>
      
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="list.php">Log Data</a>
          </li>
          <li class="breadcrumb-item active">List</li>
        </ol>
        
        <!-- Page Content -->
        <div class="card mb-3">
           <div class="card-header">
            <i class="fas fa-table"></i>
            Log Data FingerPrint 
	            <div style="float: right;">
	            	<a class="btn btn-success btn-sm" href="log_all.php"> <i class="fas fa-download"></i>
	            All Logs</a>
	            </div>
        	</div>

          <div class="card-body">
            <?php if (isset($_GET['id_fp'])): ?>
              <?php 
              $id_fp = $_GET['id_fp'];
              $sql = "SELECT * FROM tbl_hardware WHERE id_fp = '$id_fp'";
              $query = mysqli_query($conn,$sql);
              if ($query) {
                  foreach ($query as $key) {
                    $ip = $key['ip'];
                  }
                }
               ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong>
                  tidak dapat terhubung ke <?php echo $ip; ?>:80 koneksi gagal karena pihak yang terhubung tidak merespons dengan benar setelah periode waktu tertentu, atau koneksi yang dibuat gagal karena host yang terhubung telah gagal merespons.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php endif ?>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Alamat IP</th>
                    <th>Outlet</th>
                    <th>Type</th>
                    <th>Last Update</th>
                    <th>Status</th>
                    <th>Auto Run</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
               <?php 
                    $sql = "SELECT h.*,s.last_update,s.status FROM tbl_hardware h LEFT JOIN tbl_status s ON h.id_fp=s.id_fp ORDER BY ip";
                    $result = mysqli_query($conn,$sql);
                    $row = mysqli_num_rows($result);
                    if ($row > 0 ) {
                        while ($k = mysqli_fetch_array($result)) {
                            echo "<tr>
                                    <td class='hidden-phone'>".$k['id_fp']."</td>
                                    <td class='hidden-phone'>".$k['ip']."</td>
                                    <td>".$k['outlet']."</td>
                                    <td class='text-center'>".$k['type']."</td>
                                    <td class='text-center'>".$k['last_update']."</td>
                                    <td class='text-center'>"

                                    ;
                                    if ($k['status'] == 1) {
                                      echo "<span class='badge badge-success'  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Get Data Logs Berhasil\"><i class='fa fa-check-square'></i></span> <span style='display:none'>1</span>";
                                    }elseif ($k['status'] == 2){
                                     echo "<span class='badge badge-info'  data-toggle=\"tooltip\" data-placement=\"left\" title=\"Data Logs Kosong\"><i class='fa fa-info-circle'></i></span><span style='display:none'>2</span>";
                                    }elseif ($k['status'] == 3){
                                      echo "<span class='badge badge-danger' data-toggle=\"tooltip\" data-placement=\"left\" title=\"Get Data Logs Gagal\"><i class='fa fa-window-close'></i></span><span style='display:none'>3</span>";
                                    }else{

                                    }

                                  echo "</td>
                                    <td class='text-center'>";
                                    if ($k['run_interval'] == 1) {
                                      echo "<span class='badge badge-success'><i class='fa fa-check'></i></span> <span style='display:none'>1</span>";
                                    }else{
                                     echo "<span class='badge badge-danger'><i class='fa fa-minus'></i></span><span style='display:none'>0</span>";
                                    }

                                  echo "</td>
                                    <td class='text-center' style='width:150px'>
                              			<a href='get_log.php?id_fp=".$k['id_fp']."' ><button data-toggle=\"tooltip\" data-placement=\"left\" title=\"Get Data Logs\" class='btn btn-primary'  style='margin-right:5px;'> <i class='fas fa-fw fa-download'></i></button></a>
                                    <button data-toggle=\"tooltip\" data-placement=\"left\" title=\"Get Data Logs And Clear\" onclick='modalConfirm(\"".$k['id_fp']."\")' class='btn btn-danger'  style='margin-right:5px;'> <i class='fas fa-fw fa-recycle'></i></button>
                                    </td>
                                  </tr>";
                             
                        }

                    }

             	?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../asset/vendor/datatables/jquery.dataTables.js"></script>
  <script src="../asset/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../asset/js/demo/datatables-demo.js"></script>
	<script type="text/javascript">
     	$(document).ready(function() {
	      $('#logdata').addClass('active');

	    });
        function modalConfirm(id_fp) {
  
         $('#deleteModal').modal('show');
         $("#delete_").attr("href", 'get_log_and_clear.php?id_fp='+id_fp);
         $("#body_").html("Log data dalam Device "+id_fp+" akan di hapus. <br> Data yang sudah dihapus tidak dapat dikembalikan!")
         $("#delete_").html("<i class='fas fa-fw fa-recycle'></i>Get Data And Clear");
        }
        
 	</script>
</body>

</html>
