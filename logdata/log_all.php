<!DOCTYPE html>
<html lang="en">
  <?php include '../config.php'; ?>
  <?php include '../header.php'; ?>
<body id="page-top">
  <?php include '../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../sidebar.php'; ?>
    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="log_all.php">Log</a>
          </li>
          <li class="breadcrumb-item active">List</li>
        </ol>
        <!-- Page Content -->
        <div class="card mb-3">
           <div class="card-header">
            <i class="fas fa-table"></i>
            Log ALL Data FingerPrint 
            <div style="float: right;" id="button_get">
                <button class="btn btn-success btn-sm" onclick="getLogData()"> <i class="fas fa-download"></i>
              Get All Logs</button>
              <button class="btn btn-success btn-sm" onclick="uploadModal()"> <i class="fas fa-upload"></i>
              Upload Log Data</button>
              </div>
          </div>
          <?php if (isset($_GET['page_limit'])) {
            $page_limit = $_GET['page_limit'];
            $add = '&page_limit='.$page_limit;
          }else{
            $page_limit = 10;
            $add = '';
          } ?>
          <div class="card-body">
            <div id="alert"></div>
            <div class="form-group">
              <select id="page_limit" class="form-control" style="width: 100px;">
                <option <?php if ($page_limit==10) { echo "selected"; } ?> value="10">10</option>
                <option <?php if ($page_limit==25) { echo "selected"; } ?> value="25">25</option>
                <option <?php if ($page_limit==50) { echo "selected"; } ?> value="50">50</option>
                <option <?php if ($page_limit==100) { echo "selected"; } ?> value="100">100</option>
              </select>
            </div>
            <div class="table-responsive">
              <table class="table table-bordered" id="zero_config" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Device</th>
                    <th>PIN</th>
                    <th>DateTime</th>
                    <th>Verified</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>

                  <?php

                      if (isset($_GET['pageno'])) {
                          $pageno = $_GET['pageno'];
                      } else {
                          $pageno = 1;
                      }
                      if (isset($_GET['page_limit'])) {
                           $no_of_records_per_page = $_GET['page_limit'];
                      } else {
                           $no_of_records_per_page = 10;
                      }
                     
                      $offset = ($pageno-1) * $no_of_records_per_page;


                      $total_pages_sql = "SELECT COUNT(*) FROM tbl_log ";
                      $result = mysqli_query($conn,$total_pages_sql);
                      $total_rows = mysqli_fetch_array($result)[0];
                      $total_pages = ceil($total_rows / $no_of_records_per_page);

                      $sql = "SELECT * FROM tbl_log ORDER BY `datetime` DESC LIMIT $offset, $no_of_records_per_page ";
                      $res_data = mysqli_query($conn,$sql);
                      $no=$offset+1;
                      while($k = mysqli_fetch_array($res_data)){
                       echo "<tr>
                                <td class='hidden-phone'>".$no++."</td>
                                <td class='hidden-phone'>".$k['id_fp']."</td>
                                <td class='hidden-phone'>".$k['pin']."</td>
                                <td class='hidden-phone'>".$k['datetime']."</td>
                                <td class='hidden-phone'>".$k['verified']."</td>
                                <td class='hidden-phone'>".$k['status']."</td>
                                
                              </tr>";
                         $created_at = $k['created_at'];
                      }
                    //  mysqli_close($conn);
                  ?>
                  
              </table>
              <ul class="pagination">
                  <li class="paginate_button page-item"><a class="page-link" href="?pageno=1<?php echo $add; ?>">First</a></li>
                  <li class="paginate_button page-item <?php if($pageno <= 1){ echo 'disabled'; } ?>">
                      <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1).$add; } ?>">Prev</a>
                  </li>
                  <?php 
                    if($pageno != 1){
                      echo '<li class="paginate_button page-item"><a class="page-link" href="?pageno='.($pageno-1).$add.'">'.($pageno-1).'</a></li>';
                      echo '<li class="paginate_button page-item active"><a class="page-link" href="?pageno='.$pageno.$add.'">'.$pageno.'</a></li>';
                     
                      }
                    for($i = $pageno+1; $i < $pageno+3; $i++){
                     echo '<li class="paginate_button page-item"><a class="page-link" href="?pageno='.$i.$add.'">'.$i.'</a></li>';
                    }
                    echo '<li class="paginate_button page-item"><a class="page-link">...</a></li>';

                    for($i = $total_pages-2; $i <= $total_pages; $i++){
                       echo '<li class="paginate_button page-item"><a class="page-link" href="?pageno='.$i.$add.'">'.$i.'</a></li>';
                    }

                   ?>
                  <li class="paginate_button page-item <?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                      <a class="page-link" href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1).$add; } ?>">Next</a>
                  </li>
                  <li class="paginate_button page-item"><a class="page-link"   href="?pageno=<?php echo $total_pages.$add; ?>">Last</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="modal fade" id="myModalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalDeleteLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="myModalDeleteLabel">Upload Log Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <form method="POST" enctype="multipart/form-data" action="upload.php" onsubmit="closeModal();">
                      <div class="modal-body">
                          <div class="form-group">
                              <select class="form-control input-group-success" required="" name="id_fp" style="min-width: 165px;">
                                 <option value="">Pilih Data Fingerprint</option>
                                 <?php 
                                      
                                     $sql2 = "SELECT id_fp,outlet FROM tbl_hardware ORDER BY id_fp";
				     $result2 = mysqli_query($conn,$sql2);
				     if($result){
                                      $row = mysqli_num_rows($result2);
                                      if ($row > 0 ) {
                                          while ($k = mysqli_fetch_array($result2)) {
                                                 echo "<option value='".$k['id_fp']."'>".$k['id_fp']." - ".$k['outlet']."</option>";
                                             
                                          }
                                     }
				    }else{
					$row =  mysqli_error($conn);
				   }
                                  ?>
                              </select>
                          </div>
                          <div class="input-group input-group-success">
                              <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-upload"></i></label></span>
                              <input type="file" accept=".csv" class="form-control" required="" name="File" style="padding-top: 5px" placeholder="input-group-success">
                          </div>
                           <p class="help-block">File yang di upload harus sesuai dengan template dan berupa file Ms.Excel (.csv).</p>

                          <a id="urlDelete" href="download.php"><button type="button" class="btn btn-success"><i class="ik ik-download"></i> Download Template</button></a>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-success">Upload</button>
                      </div>   
                  </form>
                  
              </div>
          </div>
      </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/datatables/jquery.dataTables.js')?>"></script>
  <script src="<?php echo url('asset/vendor/datatables/dataTables.bootstrap4.js')?>"></script>

  <!-- Demo scripts for this page-->
  <script src="../asset/js/demo/datatables-demo.js"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        $('#logalldata').addClass('active');
        $('#page_limit').change(function(
          ) {
          var x = $('#page_limit').val();
          window.location.href = '?pageno=<?php echo $pageno ?>&page_limit='+x;
        });

      });
       // var table = $('#zero_config').DataTable({
       //  "processing": true,
       //  "language": {
       //      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
       //          ajax: {
       //              url: '<?php //echo url('logdata/get_log_api.php');?>',
       //              dataSrc: 'data'
       //          },
       //          columns: [ 
       //              { data: null },
       //              { data: 'id_fp' },
       //              { data: 'pin' },
       //              { data: 'datetime' },
       //              { data: 'verified' },
       //              { data: 'status' },
                   
       //           ],
       //          "columnDefs": [ {
       //              "searchable": false,
       //              "orderable": false,
       //              "targets": 0
       //          },
       //          { className: "text-center", "targets": [ 0 ] },
       //          { className: "text-center", "targets": [ 2 ] }
       //           ],
       //          "order": [[ 1, 'asc' ]],
       //  });
       //  table.on( 'order.dt search.dt', function () {
       //      table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       //          cell.innerHTML = i+1;
       //      } );
       //  } ).draw();
        function deleteModal(id_fp) {
  
         $('#deleteModal').modal('show');
         $("#delete_").attr("href", 'delete.php?id_fp='+id_fp);
        }
        
        function getLogData(){
          $('#button_get').html('<button class="btn btn-sm btn-success" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><span> Loading...</span></button>');
          $.ajax({
              url: '<?php echo url("logdata/get_log_all.php") ?>',
              type: 'POST',
              data: {},
              success: function(data) {
                  $('#button_get').html('<button class="btn btn-success btn-sm" onclick="getLogData()"> <i class="fas fa-download"></i> Get All Logs</button>');

                  $('#alert').html('<div class="alert alert-success" role="alert"><button type="button" class="close" onclick="clearbtn()" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Data : </strong> Success : '+data.data_json.success+' Error : '+data.data_json.error+'</div>');
                  clear();
              },
              error: function() {
                   $('#button_get').html('<button class="btn btn-success btn-sm" onclick="getLogData()"> <i class="fas fa-download"></i> Get Log All</button>');
              }
          });
        }
        function clear(){
          window.setTimeout(function() {
           clearbtn();
          }, 5000);
        }

        function clearbtn(){
          $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
        }
        function uploadModal(){
    
           $('#myModalUpload').modal('show');
          }

  </script>
</body>

</html>
