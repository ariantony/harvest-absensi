<?php 
require_once '../connect.php';
if (isset($_GET['id_fp'])) {
	$id_fp = $_GET['id_fp'];
    $sql = "SELECT * FROM tbl_hardware WHERE id_fp = '$id_fp'";
    $created_at = date('Y-m-d H:i:s');
	$created_by =  "System";
	$query = mysqli_query($conn,$sql);
	if ($query) {
      foreach ($query as $key) {
        $ip = $key['ip'];
        $c_key = $key['c_key'];
        $run_interval = $key['run_interval'];
      }
      $last_update = date('Y-m-d h:i:s');
      	//mengambil log data yang ada di device
		$Connect = fsockopen($ip, "80", $errno, $errstr, 1);
		if($Connect){
			$soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$c_key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
			$newLine="\r\n";
			fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
		    fputs($Connect, "Content-Type: text/xml".$newLine);
		    fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
		    fputs($Connect, $soap_request.$newLine);
			$buffer="";
			while($Response=fgets($Connect, 1024)){
				$buffer=$buffer.$Response;
			}
			
			$buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
			$buffer=explode("\r\n",$buffer);
		 	if (count($buffer) > 2) {
		 			$status_ = 1;
					for($a=1;$a<count($buffer)-1;$a++){
						$data=Parse_Data($buffer[$a],"<Row>","</Row>");
						$PIN=Parse_Data($data,"<PIN>","</PIN>");
						$DateTime=Parse_Data($data,"<DateTime>","</DateTime>");
						$Verified=Parse_Data($data,"<Verified>","</Verified>");
						$Status=Parse_Data($data,"<Status>","</Status>");

						$sql = "INSERT IGNORE INTO tbl_log(`id_fp`, `pin`,`datetime`,`verified`,`status`, `created_at`, `created_by`) VALUES ('$id_fp', '$PIN', '$DateTime','$Verified','$Status', '$created_at', '$created_by')";
						$result = mysqli_query($conn,$sql);

				 	}
				 		$sql2 = "INSERT IGNORE INTO tbl_status(`id_fp`,`last_update`,`status`) VALUES ('$id_fp', '$last_update','$status_')";
						$result2 = mysqli_query($conn,$sql2);
						if ($result2) {
							$sql3 = "UPDATE tbl_status SET `last_update`='$last_update',`status`='$status_' WHERE id_fp='$id_fp'";
							$result3 = mysqli_query($conn,$sql3);
						}
							//Digunakan untuk mengapus log data yang berada di device
						$Connect = fsockopen($ip, "80", $errno, $errstr, 1);
						if($Connect){
							$soap_request="<ClearData><ArgComKey xsi:type=\"xsd:integer\">".$c_key."</ArgComKey><Arg><Value xsi:type=\"xsd:integer\">3</Value></Arg></ClearData>";
							$newLine="\r\n";
							fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						    fputs($Connect, "Content-Type: text/xml".$newLine);
						    fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						    fputs($Connect, $soap_request.$newLine);
							$buffer="";
							while($Response=fgets($Connect, 1024)){
								$buffer=$buffer.$Response;
							}
							$buffer=Parse_Data($buffer,"<Information>","</Information>");
							header('location:detail.php?id_fp='.$id_fp.'&status='.$buffer);
						}else{
							header('location:list.php?id_fp='.$id_fp);
						}
						
						header('location:detail.php?id_fp='.$id_fp);
					
		 	}else{
		 		$status_ = 2;
		 		$sql2 = "INSERT IGNORE INTO tbl_status(`id_fp`,`last_update`,`status`) VALUES ('$id_fp', '$last_update','$status_')";
						$result2 = mysqli_query($conn,$sql2);
				if ($result2) {
					$sql3 = "UPDATE tbl_status SET `last_update`='$last_update',`status`='$status_' WHERE id_fp='$id_fp'";
					$result3 = mysqli_query($conn,$sql3);
				}
		 		header('location:detail.php?id_fp='.$id_fp.'&data=0');
		 	}
		}else{
			$status_ = 3;
			$sql2 = "INSERT IGNORE INTO tbl_status(`id_fp`,`last_update`,`status`) VALUES ('$id_fp', '$last_update','$status_')";
			$result2 = mysqli_query($conn,$sql2);
			if ($result2) {
				$sql3 = "UPDATE tbl_status SET `last_update`='$last_update',`status`='$status_' WHERE id_fp='$id_fp'";
				$result3 = mysqli_query($conn,$sql3);
			}
			header('location:list.php?id_fp='.$id_fp);
		}


	}else{
		echo "Error : ".mysqli_error($conn);
	}
}
		
function Parse_Data($data,$p1,$p2){
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;	
	}

 ?>