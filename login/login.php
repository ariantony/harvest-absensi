<!DOCTYPE html>
<html lang="en">
<?php 
function url($url)
  {
    $host = 'http://'.$_SERVER['HTTP_HOST'].'/absen/basima/';
    return $host.$url ;
  } ?>
	  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Basima Admin</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo url('asset/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css');?>">
  <!-- Custom styles for this template-->
  <link href="<?php echo url('asset/css/sb-admin.css');?>" rel="stylesheet">
<body class="bg-dark">

  <div id="wrapper">

    <!-- Sidebar -->
    <div id="content-wrapper">

      <div class="container">
        <div class="card card-login mx-auto mt-5">
          <div class="card-header">Login</div>
          <div class="card-body">
            <?php if (isset($_GET['er'])): ?>
              <?php if ($_GET['er'] == 1): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong> Password yang anda masukan salah.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php endif ?>
              <?php if ($_GET['er'] == 2): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong> Username tidak terdaftar.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php endif ?>
            <?php endif ?>
            <form action="auth.php" method="POST">
              <div class="form-group">
                <div class="form-label-group">
                  <input type="text" id="username" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                  <label for="username">Username</label>
                </div>
              </div>
              <div class="form-group">
                <div class="form-label-group">
                  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                  <label for="inputPassword">Password</label>
                </div>
              </div>
              <button class="btn btn-primary btn-block" type="submit">Login</button>
            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

</body>

</html>
