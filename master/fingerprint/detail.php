<!DOCTYPE html>
<html lang="en">
  <?php include '../../config.php'; ?>
  <?php include '../../header.php'; ?>
<body id="page-top">
  <?php include '../../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../../sidebar.php'; ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">FingerPrint</a>
          </li>
          <li class="breadcrumb-item active">List</li>
        </ol>

        <!-- Page Content -->
        <div class="card mb-3">
           <div class="card-header">
            <i class="fas fa-table"></i>
            Data FingerPrint 
	            <div style="float: right;">
	            	<a class="btn btn-success btn-sm" href="list.php"> <i class="fas fa-list"></i>
	            List</a>
	            </div>
        	</div>

          <div class="card-body">
            <?php if (isset($_GET['error'])): ?>
               <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong> Data Device <?php echo $_GET['error']; ?> tidak dapat di hapus karena digunakan sebagai foreign key di tabel lain.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            <?php endif ?>
            <div class="table-responsive">
              <table class="table table-bordered" width="100%" cellspacing="0">
                  <?php 
               		$id_fp = $_GET['id_fp'];
                    $sql = "SELECT * FROM tbl_hardware WHERE id_fp='$id_fp'";
                    $result = mysqli_query($conn,$sql);
                    $row = mysqli_num_rows($result);
                    if ($row > 0 ) {
                        while ($k = mysqli_fetch_array($result)) {
                            echo "<thead>
					                  <tr>
					                    <th>ID</th>
					                    <th>".$k['id_fp']."</th>
					                  </tr>
					                  <tr>
					                    <th>Alamat IP</th>
					                    <th>".$k['ip']."</th>
					                  </tr>
					                  <tr>
					                    <th>Common Key</th>
					                    <th>".$k['c_key']."</th>
					                  </tr>
					                  <tr>
					                    <th>Brand</th>
					                    <th>".$k['brand']."</th>
					                  </tr>
					                  <tr>
					                    <th>Outlet</th>
					                    <th>".$k['outlet']."</th>
					                  </tr>
					                  <tr>
					                    <th>Machine</th>
					                    <th>".$k['machine']."</th>
					                  </tr>
					                  <tr>
					                    <th>Type</th>
					                    <th>".$k['type']."</th>
					                  </tr>
					                  <tr>
					                    <th>Auto Run</th>
					                    <th>".$k['run_interval']."</th>
					                  </tr>
					                  <tr>
					                    <th>Serial Number</th>
					                    <th>".$k['number_fp']."</th>
					                  </tr>
					                  <tr>
					                    <th>Description</th>
					                    <th>".$k['desc']."</th>
					                  </tr>
					                  <tr>
					                    <th>Created By</th>
					                    <th>".$k['created_by']."</th>
					                  </tr>
					                  <tr>
					                    <th>Action</th>
					                    <th><a href='add.php?id_fp=".$k['id_fp']."' ><button class='btn btn-primary'  style='margin-right:5px;'> <i class='fas fa-fw fa-edit'></i> Edit</button></a></th>
					                  </tr>
					                </thead>";
					                $created_at = $k['created_at'];
					            }
					        }
				         	?>
                
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated at <?php echo $created_at; ?></div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../../asset/vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../asset/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../../asset/js/demo/datatables-demo.js"></script>
	<script type="text/javascript">
     	$(document).ready(function() {
        $('#fingerprint_list').addClass('active');
        $('#fingerprint_').addClass('active');
	      $('#fingerprint_').addClass('show');
        $('#fingerprint').click();
	      

	    });
        function deleteModal(id_fp) {
  
         $('#deleteModal').modal('show');
         $("#delete_").attr("href", 'delete.php?id_fp='+id_fp);
        }
        
 	</script>
</body>

</html>
