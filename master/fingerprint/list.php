<!DOCTYPE html>
<html lang="en">
  <?php include '../../config.php'; ?>
  <?php include '../../header.php'; ?>
<body id="page-top">
  <?php include '../../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../../sidebar.php'; ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">FingerPrint</a>
          </li>
          <li class="breadcrumb-item active">List</li>
        </ol>

        <!-- Page Content -->
        <div class="card mb-3">
           <div class="card-header">
            <i class="fas fa-table"></i>
            Data FingerPrint 
	            <div style="float: right;">
	            	<a class="btn btn-success btn-sm" href="add.php"> <i class="fas fa-plus"></i>
	            Tambah</a>
	            </div>
        	</div>

          <div class="card-body">
            <?php if (isset($_GET['error'])): ?>
               <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Perhatian! : </strong> Data Device <?php echo $_GET['error']; ?> tidak dapat di hapus karena digunakan sebagai foreign key di tabel lain.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            <?php endif ?>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Alamat IP</th>
                    <th>Common Key</th>
                    <th>Outlet</th>
                    <th>Type</th>
                    <th>Auto Run</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
               <?php 
                    $sql = "SELECT * FROM tbl_hardware ORDER BY ip";
                    $result = mysqli_query($conn,$sql);
                    $row = mysqli_num_rows($result);
                    if ($row > 0 ) {
                        while ($k = mysqli_fetch_array($result)) {
                            echo "<tr>
                                    <td class='hidden-phone'>".$k['id_fp']."</td>
                                    <td class='hidden-phone'>".$k['ip']."</td>
                                    <td class='text-center'>".$k['c_key']."</td>
                                    <td class='hidden-phone'>".$k['outlet']."</td>
                                    <td class='hidden-phone'>".$k['type']."</td>
                                    <td class='text-center'>";
                                    if ($k['run_interval'] == 1) {
                                      echo "<span class='badge badge-success'><i class='fa fa-check'></i></span> <span style='display:none'>1</span>";
                                    }else{
                                     echo "<span class='badge badge-danger'><i class='fa fa-minus'></i></span><span style='display:none'>0</span>";
                                    }

                                  echo "</td>
                                    <td class='text-center'>
                              			
                                    <a href='detail.php?id_fp=".$k['id_fp']."' ><button class='btn btn-primary'  style='margin-right:5px;'> <i class='fas fa-fw fa-eye'></i></button></a>";
                              echo '<button onclick="deleteModal(\''.$k['id_fp'].'\')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </td>
                          </tr>';
                          $created_at = $k['created_at'];

                        }
                    }

             	?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated at <?php echo $created_at; ?></div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../../asset/vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../asset/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../../asset/js/demo/datatables-demo.js"></script>
	<script type="text/javascript">
     	$(document).ready(function() {
        $('#fingerprint_list').addClass('active');
        $('#fingerprint_').addClass('active');
	      $('#fingerprint_').addClass('show');
        $('#fingerprint').click();
	      

	    });
        function deleteModal(id_fp) {
  
         $('#deleteModal').modal('show');
         $("#delete_").attr("href", 'delete.php?id_fp='+id_fp);
         $("#body_").html("Device "+id_fp+" akan di hapus. <br> Data yang sudah dihapus tidak dapat dikembalikan!")
        }
        
 	</script>
</body>

</html>
