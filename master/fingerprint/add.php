<!DOCTYPE html>
<html lang="en">
  <?php include '../../config.php'; ?>
  <?php include '../../header.php'; ?>
<body id="page-top">
  <?php include '../../navbar.php'; ?>

  <div id="wrapper">

    <!-- Sidebar -->
  <?php include '../../sidebar.php'; ?>

    <div id="content-wrapper">

      <div class="container-fluid">
        <?php if (isset($_GET['id_fp'])) {
          $id_fp = $_GET['id_fp'];
          $sql = "SELECT * FROM tbl_hardware WHERE id_fp = '$id_fp'";
          $query = mysqli_query($conn,$sql);
          foreach ($query as $key) {
            $ip = $key['ip'];
            $c_key = $key['c_key'];
            $number_fp = $key['number_fp'];
            $desc = $key['desc'];
            $run_interval = $key['run_interval'];
            $brand = $key['brand'];
            $outlet = $key['outlet'];
            $machine = $key['machine'];
            $type = $key['type'];
          }
        } ?>
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="list.php">Fingerprint</a>
          </li>
          <li class="breadcrumb-item active">Tambah</li>
        </ol>

        <!-- Page Content -->
        <div class="card mx-auto mt-5">
	      <div class="card-header">Tambah Data FingerPrint
          <div style="float: right;">
                <a class="btn btn-success btn-sm" href="list.php"> <i class="fas fa-list"></i>
              List</a>
              </div>
        </div>

	      <div class="card-body">
	        <form action="add_save.php" method="POST">
            <input type="hidden" name="id_fp" value="<?php if (isset($_GET['id_fp'])) { echo $id_fp;}?>">
	          <div class="form-group">
	            <div class="form-row">
	              <div class="col-md-6">
	                <div class="form-label-group">
	                  <input type="text" id="ip" name="ip" maxlength="16" class="form-control" placeholder="Alamat IP" value="<?php if (isset($_GET['id_fp'])) { echo $ip;}?>" required="required" autofocus="autofocus">
	                  <label for="ip">Alamat IP</label>
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-label-group">
	                  <input type="text" id="key" maxlength="10" value="<?php if (isset($_GET['id_fp'])) { echo $c_key;}?>" name="key" class="form-control" placeholder="Common Key" required="required">
	                  <label for="key">Common Key</label>
	                </div>
	              </div>
	            </div>
	          </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="brand" name="brand" maxlength="4" class="form-control" placeholder="Brand" value="<?php if (isset($_GET['id_fp'])) { echo $brand;}?>" required="required" autofocus="autofocus">
                    <label for="brand">Brand</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="outlet" maxlength="50" value="<?php if (isset($_GET['id_fp'])) { echo $outlet;}?>" name="outlet" class="form-control" placeholder="Outlet" required="required">
                    <label for="outlet">Outlet</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="machine" maxlength="30" value="<?php if (isset($_GET['id_fp'])) { echo $machine;}?>" name="machine" class="form-control" placeholder="Machine Name" required="required">
                    <label for="machine">Machine Name</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="type" maxlength="15" value="<?php if (isset($_GET['id_fp'])) { echo $type;}?>" name="type" class="form-control" placeholder="Machine Type" required="required">
                    <label for="type">Machine Type</label>
                  </div>
                </div>
              </div>
            </div>
	          <div class="form-group">
	            <div class="form-label-group">
	              <input type="text" maxlength="30" value="<?php if (isset($_GET['id_fp'])) { echo $number_fp;}?>" id="number_fp" name="number_fp" class="form-control" placeholder="Nomor Seri">
	              <label for="number_fp">Nomor Seri</label>
	            </div>
	          </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" maxlength="100" value="<?php if (isset($_GET['id_fp'])) { echo $desc;}?>" id="desc" name="desc" class="form-control" placeholder="Keterangan">
                <label for="desc">Keterangan</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="checkbox" <?php if(isset($_GET['id_fp'])){if($run_interval == 1) { echo 'checked';}}?> id="run_interval" name="run_interval" class="form-control">
                <label for="run_interval">Auto Run</label>
              </div>
            </div>
            <?php if (isset($_GET['id_fp'])): ?>
              <button class="btn btn-success btn-block" type="submit" name="update">Update</button>
            <?php endif ?>
            <?php if (!isset($_GET['id_fp'])): ?>
              <button class="btn btn-primary btn-block" type="submit" name="add">Tambah</button>
            <?php endif ?>
	        </form>
	      </div>
	    </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
    <?php include '../../footer.php'; ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo url('asset/vendor/jquery-easing/jquery.easing.min.js')?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo url('asset/js/sb-admin.min.js')?>"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        $('#fingerprint_add').addClass('active');
        $('#fingerprint_').addClass('active');
        $('#fingerprint_').addClass('show');
        $('#fingerprint').click();

      });
        
  </script>
</body>

</html>
